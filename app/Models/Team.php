<?php

namespace Models;

class Team {

	private $api;

	private $endpoints = [];
	private $teams     = [];
	private $players   = [];

	public function __construct()
	{
		$this->api = new \Core\ApiParser();
		$team_endpoints = $this->api->getEndpointListGroup('team');

		foreach ($team_endpoints as $name => $endpoint) {
			$this->endpoints[$name] = $endpoint;
		}

		// (new System())->debug($this->endpoints);
	}

	public function getTeams($params = [])
	{
		$teams = $this->api->pullData($this->endpoints['teams'])->league->standard;
		foreach ($teams as $team) {
			if (!empty($params) && count($params) > 0) {
				foreach ($params as $key => $param) {
					if ($team->{$key} != $param) {
						// echo "skip me";
						continue 2;
					}
				}
			}
			$this->teams[$team->teamId] = [
				'name'             => $team->fullName,
				'tricode'          => $team->tricode,
				'division'         => $team->divName,
				'conference'       => $team->confName,
				'city'             => $team->city,
				'is_nba_franchise' => $team->isNBAFranchise,
			];
		}

		return $this->teams;
	}

	public function getTeamRoster($teamId)
	{
		$teamRoster = $this->api->pullData($this->endpoints['teamRoster'], ['teamUrlCode' => $teamId])->league->standard;
		$players    = $teamRoster->players;
		return $players;
	}

	public function getTeamSchedule($teamId,$year = '')
	{
		$year = (!empty($year)) ? $year : date('Y');
		$data = [
			'seasonScheduleYear' => $year,
			'teamUrlCode'        => $teamId
		];
		$teamSchedule = $this->api->pullData($this->endpoints['teamScheduleYear'], $data)->league->standard;

		$formattedData = [
		];

		foreach ($teamSchedule as $key => $game) {
			$formattedData[$game->gameId] = [
				'isHomeTeam' => $game->isHomeTeam,
				'startDate'  => $game->startDateEastern,
				'startTime'  => $game->startTimeUTC,
				'type'       => $game->nugget->text,
				'thisTeam'   => [
					'id'    => $game->vTeam->teamId,
					'score' => $game->vTeam->score,
				],
				'opposingTeam' => [
					'id'    => $game->hTeam->teamId,
					'score' => $game->hTeam->score,
				]
			];
		}

		return $formattedData;
	}
}