<?php

namespace Models;

class Scoreboard {
	private $api;
	private $endpoints = [];

	public function __construct()
	{
		$this->api = new \Core\ApiParser;
		$this->endpoints = $this->api->getEndpointListGroup('score');

		// (new \Core\System())->debug($this->endpoints);
	}

	public function getScoreboard($date = '', $params = [])
	{

		$domestic   = isset($params['domestic']) ? $params['domestic'] : true;
		$date       = (empty($date)) ? date('Ymd') : $date;
		$score      = [];

		$scoreboard = $this->api->pullData($this->endpoints['todayScoreboard'],['gameDate' => $date])->games;

		if ($domestic) {
			$scoreboard = array_filter($scoreboard, function($v) {
				if ($v->arena->isDomestic) {
					return $v;
				}
			});
		}

		if (!empty($params['teamId'])) {
			$scoreboard = array_filter($scoreboard, function($v) use ($params) {
				if ($v->vTeam->teamId == $params['teamId'] || $v->hTeam->teamId == $params['teamId']) {
					return $v;
				}
			});
		}

		foreach ($scoreboard as $board) {
			$score[] = [
				'arena'        => $board->arena->name,
				'city'         => $board->arena->city,
				'gameDuration' => [
					'hours'   => $board->gameDuration->hours,
					'minutes' => $board->gameDuration->minutes,
				],
				'vteam' => [
					'teamId' => $board->vTeam->teamId,
					'score'  => $board->vTeam->score,
				],
				'hteam' => [
					'teamId' => $board->hTeam->teamId,
					'score'  => $board->hTeam->score,
				],
			];
		}

		(new \Core\System())->debug($score);

		return $score;
	}
}