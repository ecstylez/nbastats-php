<?php

namespace Models;

class Player {

	private $api;
	private $endpoints = [];
	private $members   = [];

	public function __construct()
	{
		$this->api = new \Core\ApiParser();
		$this->endpoints = $this->api->getEndpointListGroup('Player');

		// (new \Core\System())->debug($this->endpoints);
	}

	public function getPlayerProfile($playerId)
	{
		$player = $this->api->pullData($this->endpoints['playerProfile'], ['personId' => $playerId])->league->standard;

		// (new \Core\System())->debug($player);
		return $player;
	}

	public function getRosterPlayers($teamId = '')
	{
		$players = $this->api->pullData($this->endpoints['leagueRosterPlayers'])->league->standard;
		$members = [];

		if (!empty($teamId)) {
			foreach ($players as $key => $player) {
				if ($player->teamId == $teamId) {
					$members[$player->personId] = [
						'firstName' => $player->firstName,
						'lastName'  => $player->lastName,
						'active'    => $player->isActive,
						'position'  => $player->pos,
						'jersey'    => $player->jersey,
						'dob'       => $player->dateOfBirthUTC,
					];
				}
			}
 		}

 		$this->members = $members;

 		return $this->members;
	}
}
