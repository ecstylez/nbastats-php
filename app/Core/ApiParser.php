<?php
	namespace Core;

	class ApiParser {

		const DATA_HOST    = 'http://data.nba.net/10s/';
		const ENDPOINT_DIR = 'prod/v1/today.json';

		public $endpoints = [];

		public function __construct()
		{
			$this->getEndpoints();
		}

		private function getEndpoints()
		{
			$api       = file_get_contents(self::DATA_HOST . '/' . self::ENDPOINT_DIR);
			$endpoints = (array) json_decode($api)->links;

			foreach ($endpoints as $key => $endpoint) {
				if (strpos($endpoint, '.json') !== false) {
					$this->endpoints[$key] = $endpoint;
				}
			}

			// (new \Core\System)->debug($this->endpoints);
		}

		public function getEndpointListGroup($groupName)
		{
			$endpoints = array_values(array_filter(array_map(function($d, $k) use ($groupName) {
				if (strpos(strtolower($k), strtolower($groupName)) !== false) {
					return [$k => $d];
				}
			}, $this->endpoints, array_keys($this->endpoints))));

			$endpointGroup = [];
			foreach ($endpoints as $endpoint_array) {
				foreach ($endpoint_array as $key => $endpoint) {
					$endpointGroup[$key] = $endpoint;
				}
			}

			return $endpointGroup;
		}

		public function pullData($endpoint_dir, $params = [])
		{
			if (sizeof($params) > 0) {
				foreach ($params as $key => $param) {
					$endpoint_dir = str_replace('{{' . $key . '}}', $param, $endpoint_dir);
				}
			}
			$data = json_decode(file_get_contents(self::DATA_HOST . '/' . $endpoint_dir));
			return $data;
		}
	}
