<?php

namespace Core;

class System {

	private $debug;

	public function __construct($debug = false)
	{
		$this->debug = $debug;
	}

	public function debug($data)
	{
		$backtrace = debug_backtrace()[0];
		echo '<pre>';
		echo '<strong>Called on ' . basename($backtrace['file']) . ' on line ' . $backtrace['line'] . '</strong>' . PHP_EOL . PHP_EOL;
		echo var_export($data,true);
		echo '</pre>';
	}
}
