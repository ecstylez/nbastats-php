<?php

class Autoloader {

	public function __construct()
	{
		// load controller
		$this->loadController();

		// load dependencies
		spl_autoload_register([$this, 'loader']);
	}

	private function loadController()
	{
		require 'Controller.php';
	}

	private function loader($className)
	{
		$directories = $this->getCoreDirectories();

		$slash     = strrpos($className, '\\');
		$namespace = substr($className, 0, $slash);
		$class     = substr($className, ($slash > 1) ? $slash + 1 : $slash);

		foreach ($directories as $directory) {
			if (file_exists(__DIR__ . "/" . $directory . "/" . $class . ".php")) {
				require $directory . '/' . $class . '.php';
			}
		}
	}

	private function getCoreDirectories()
	{
		$directories = array_map(function($line) {
				if (strpos($line, '.') === false) {
					return $line;
				}
			},
			scandir(__DIR__)
		);

		return array_values(array_filter($directories));
	}
}
