<?php

namespace Helpers;

class HtmlHelper
{

	public function link($value, $src, $params = [])
	{
		$string = "<a href='/" . $src . "' " . $this->formatParams($params) . ">" . $value . "</a>";
		return $string;
	}

	public function image($src, $params = [])
	{
		$string = "<img src='" . $src . "' " . $this->formatParams($params) . "/>";
		return $string;
	}

	private function formatParams($params = [])
	{
		$formatted = array_map(function($v, $k) {
			return $k . " = '" . $v . "'";
		}, $params, array_keys($params));

		return implode(" ", $formatted);
	}
}