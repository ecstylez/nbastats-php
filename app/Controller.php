<?php
namespace Controller;

use Helpers\HtmlHelper;
use Core\System;

class Controller {

	private $controllerName;
	private $viewName;


	public function render($page, $data = [])
	{
		$page                 = explode("/", $page);

		$this->controllerName = $page[0];
		$this->viewName       = $page[1];

		// helpers
		$html = new \Helpers\HtmlHelper;
		$system = new \Core\System;

		require_once(__DIR__ . '/../views/templates/header.php');
		require_once(__DIR__ . '/../views/' . $this->controllerName . '/' . $this->viewName . '.php');
		require_once(__DIR__ . '/../views/templates/footer.php');
	}

}

function debug($msg) {
	(new \Core\System)->debug($msg);
}
// $path = (isset($_GET['path'])) ? explode("/",$_GET['path']) : [];
// new Controller($path);