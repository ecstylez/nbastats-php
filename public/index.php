<?php
	// autoloader
	require_once __DIR__ . '/../app/autoload.php';
	$loader = new Autoloader();

	// url parser
	// require_once __DIR__ . '';
	$path       = (isset($_GET['path'])) ? explode("/",$_GET['path']) : [];
	$controller = isset($path[0]) && !empty($path[0]) ? $path[0] : 'home';
	$action     = isset($path[1]) && !empty($path[1]) ? $path[1] : 'index';
	$params     = array_values(array_filter($path, function($v, $k) {
		if ($k >= 2) {
			return $v;
		}
	}, ARRAY_FILTER_USE_BOTH));

	// load controller and method
	require_once __DIR__ . '/../controllers/' . ucfirst($controller) . 'Controller.php';
	$className = 'Controller\\' .ucfirst($controller) . 'Controller';

	(new $className())->$action($params);
?>