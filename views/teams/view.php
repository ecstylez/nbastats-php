<article>
	What would you like to see?
</article>

<?= $html->link('Past Scores', 'teams/scores/' . $data[0], ['class' => 'btn btn-primary', 'role' => 'button']); ?>
<?= $html->link('Game Schedule', 'teams/schedule/' . $data[0], ['class' => 'btn btn-primary', 'role' => 'button']); ?>
<?= $html->link('Team Roster', 'teams/roster/' . $data[0], ['class' => 'btn btn-primary', 'role' => 'button']); ?>

<?php
	// (new \Core\System)->debug($data['roster']);
?>