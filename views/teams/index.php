<article>
	Please select a team to continue
</article>

<div class="row">
	<?php
		foreach ($data['teams'] as $teamId => $team) {
			$teamImg = 'http://cdn.nba.net/assets/logos/teams/secondary/web/' . $team['tricode'] . '.svg';
	?>
	<div class="col-md-3">
		<div class="team-logo-container">
			<div class="team-logo">
				<?= $html->link($html->image($teamImg, ['title' => $team['name']]), 'teams/view/' . $teamId); ?>
			</div>
			<div class="team-name">
				<?= $html->link($team['name'], 'teams/view/' . $teamId); ?>
			</div>
		</div>
	</div>
	<?php
		}
	?>
</div>
