<html>
<head>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<style type="text/css">
		@import 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css';

			body {
				background-color : #222;
				color            : #DDD;
				font-family      : Verdana;
				font-size        : 12px;
			}

			pre {
				border        : 1px solid #666;
				border-radius : 2px;
				padding       : 4px;
				/*display       : block;*/
				float         : left;
				margin        : 3px;
				color:#FFF;
			}
			pre:after {
				content : "";
				clear:both;
			}

			a {
				color: #DDD;
			}

			a:hover {
				text-decoration: underline;
			}

			.team-logo-container {
				text-align: center;
				padding: 10px;
				border-radius:3px;
				border:1px solid #666;
				margin-bottom: 5px;
			}
			.team-logo-container:hover {
				background: #333;
			}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<hgroup class="page-title">
					<h1>NBA GameDay</h1>
					<h3><?= (isset($data['title'])) ? $data['title'] : 'Welcome to NBA Gameday!'; ?></h3>
				</hgroup>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">