<?php

namespace Controller;

use \Models\Team as Team;
use \Models\Player as Player;

class TeamsController extends Controller
{

	public function index($params = [])
	{
		$tParams = ['isNBAFranchise' => true];

		if (isset($params[0]) && $params[0] == 'all') {
			unset($tParams['isNBAFranchise']);
		}
		$teams = (new Team())->getTeams($tParams);

		$this->render('teams/index', ['teams' => $teams]);
	}

	public function view($params = [])
	{
		// $teamRoster = (new Player())->getRosterPlayers($params[0]);

		$this->render('teams/view', $params);
	}
}
?>