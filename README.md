
# NBA Scoreboard
Written by : Mark Eccleston

## What is it?

NBA Scoreboard is a simple little app that I wrote to get NBA team stats for all teams, along with their rosters and game times.

## Languages used
PHP7, Javascript, CSS
Frameworks : none

## Want to see more?
To see more of my work, please visit [Mark Eccleston Software Engineer](http://mark-eccleston.com)